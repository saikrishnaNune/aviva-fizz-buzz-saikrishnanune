package main;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import helper.AvivaHelper;
@SpringBootApplication
public class App extends SpringBootServletInitializer
{
	/**
	 * A main method to start this application.
	 */

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Bean("avivaHelper")
	public AvivaHelper avivaHelper() {
		return new AvivaHelper();
	}
}
