package helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import model.BuzzResponse;

public class AvivaHelper {
	public BuzzResponse populateResponse(int givenValue) {
		BuzzResponse response = new BuzzResponse();
		List<String> printedList = null;
		List<String> modifiedList = new ArrayList<String>();
		String fizzWord = null;

		if (givenValue > 1 && givenValue < 1000) {
			printedList = printList(givenValue);

			for (String value : printedList) {

				fizzWord = value;
				Calendar cal = Calendar.getInstance();
				cal.get(Calendar.DAY_OF_WEEK);
				if (cal.get(Calendar.DAY_OF_WEEK) == 4) {
					
					if (value.equals("fizz buzz")) {
						fizzWord = value.replace("fizz buzz", "wizz wuzz");
					}
					if (value.equals("fizz")) {
						fizzWord = value.replace("fizz", "wizz");

					}
					if (value.equals("buzz")) {
						fizzWord = value.replace("buzz", "wuzz");
					}
				}
				modifiedList.add(fizzWord);
			}
			response.setPrintList(modifiedList);
		} else {
			response.setErrorMessage("Please Enter A number between 1 - 1000.");
		}
		return response;
	}

	public List<String> printList(int givenValue) {

		List<String> strArr = new ArrayList<String>();
		for (int i = 1; i < givenValue; i++) {
			if (i % 3 == 0) {
				if (i % 5 == 0) {
					strArr.add("fizz buzz");
				} else {
					strArr.add("fizz");
				}
			} else if (i % 5 == 0) {
				strArr.add("buzz");
			} else {
				strArr.add(String.valueOf(i));
			}

		}
		return strArr;
	}
}
