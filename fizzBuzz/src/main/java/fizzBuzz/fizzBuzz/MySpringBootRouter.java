package fizzBuzz.fizzBuzz;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import model.BuzzResponse;

@Component
public class MySpringBootRouter extends RouteBuilder {

	@Override
	public void configure() {
		//  from("timer:trigger") .transform().simple("ref:myBean") .to("log:out");
		 
	}

	@Bean
	BuzzResponse myBean() {
		BuzzResponse br = new BuzzResponse();
		// br.setValue("I'm Spring bean!");
		return br;
	}

}
