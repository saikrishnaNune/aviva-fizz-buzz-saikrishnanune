package fizzBuzz.fizzBuzz;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import helper.AvivaHelper;
import model.BuzzRequest;
import model.BuzzResponse;

@RestController
@Component
public class FizzzController {

	@Autowired
	private AvivaHelper avivaHelper;
	@Autowired
	ProducerTemplate producer;

	@RequestMapping(value = "/api/v1/aviva", method = RequestMethod.POST)
	public BuzzResponse getBuzz(@RequestBody BuzzRequest buzzRequest) {
		return avivaHelper.populateResponse(buzzRequest.getUserInput());

	}
}
