package model;

public class BuzzRequest {
	private int userInput;

	public int getUserInput() {
		return userInput;
	}

	public void setUserInput(int userInput) {
		this.userInput = userInput;
	}

}
