package model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class BuzzResponse {
	private List<String> printList;
	private String errorMessage;

	public List<String> getPrintList() {
		return printList;
	}

	public void setPrintList(List<String> printList) {
		this.printList = printList;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
